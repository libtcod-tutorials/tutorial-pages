---
title: "Tutorial 0"
summary: "Setup and \"Hello, world!\""
date: 2021-07-12
draft: false
---
For this tutorial we will go over setting up the development environment, including the libtcod tutorial project with the necessary requirements.

## Environment Setup
### Windows
Set up [Visual Studio Community][visual-studio-community] with C++ development modules, optionally with the Clang add-on.

Alternatively, set up C++ development with [Visual Studio Code][vscode].

Also install [CMake].

### Linux
Install the build tools for your distribution from the package manager (usually `build-essential` or a similar package), as well as the package for CMake. In addition, installing the SDL2 package may be necessary to work with libtcod.

For an IDE there are many options, but I personally use the open source version of Visual Studio Code for development on Linux, which can normally be found in your distro's package manager as well, but can also be installed from the link in the Windows section above. The setup for working in C++ in VSCode follows a similar set up to the Windows version.

### libtcod
Download the latest release of [libtcod][libtcod-download] for your OS and compiler. On most Windows systems, you'll probably want the `x86_64-msvc` option. 

Starting with release 1.23.1, if you are using Linux, you can download the `libtcod-1.23.1-x86_64.tar.gz` file. Otherwise you'll have to download and build from [source][libtcod-source].

## Project Setup
### Create project folder
Create a folder (I used the name `libtcod-tutorial`, but you can call it whatever you like). Then create a file called `CMakeLists.txt` and copy the following text into it:

```cmake {linenos=table}
# Set the cmake minimum version
cmake_minimum_required(VERSION 3.26)

# Name of the CMake project, description, and what languages it uses
project(LibtcodTutorials 
    DESCRIPTION "Modern C++ libtcod tutorials"
    LANGUAGES CXX
)

# Use C++20 standard for compiling
set(CMAKE_CXX_STANDARD 20)

# Add more warnings to the default compiler flags
if(WIN32)
    set(CMAKE_CXX_FLAGS "${CMAKE_CXX_FLAGS} /Wall")
else()
    set(CMAKE_CXX_FLAGS "${CMAKE_CXX_FLAGS} -pedantic -Wall -Wextra")
endif(WIN32)

## Find tcod library
find_library(tcod_LIBRARY
    NAMES libtcod
    PATHS "${CMAKE_SOURCE_DIR}/include/libtcod/"
    REQUIRED
)

## Find some required DLLs for Windows
if(WIN32)
    find_file(tcod_DLL
        NAMES libtcod.dll
        PATHS "${CMAKE_SOURCE_DIR}/include/libtcod/"
        REQUIRED
    )

    find_file(sdl_DLL
        NAMES SDL2.dll
        PATHS "${CMAKE_SOURCE_DIR}/include/libtcod/"
        REQUIRED
    )
endif(WIN32)

# Add the target to create the executable from sources
add_executable(tutorial
	main.cpp
)

# Add the include directory
target_include_directories(tutorial
    PRIVATE ${libtcod_INCLUDE_DIR}
)

# Link the libtcod library to the executable
target_link_libraries(tutorial
    ${tcod_LIBRARY}
)

# On Windows, need to copy the tcod and SDL dlls to ensure our executable runs
if(WIN32)
    configure_file("${tcod_DLL}" "${CMAKE_CURRENT_BINARY_DIR}" COPYONLY)
    configure_file("${sdl_DLL}" "${CMAKE_CURRENT_BINARY_DIR}" COPYONLY)
endif(WIN32)

# Copy the terminal.png file needed to properly render tiles
configure_file("${CMAKE_SOURCE_DIR}/terminal.png" "${CMAKE_BINARY_DIR}" COPYONLY)
```

This file will be used by CMake to allow us to build the project later. It handles setting up the compiler options, finding where the libtcod headers and libraries are (more on that later), and configuring how to build an executable.

### Unpack libtcod
In the project folder, create a folder called `libs`. Inside this folder, create another folder called `libtcod` and extract the libtcod zip into this folder.

If you are on Linux, creating a soft link to your build directory should work fine (`ln -s path/to/libtcod/directory`).

### Hello, world!
Now we're ready to get the project started. Create a file called `main.cpp` in the base folder of the project and copy the following code into it:

```cpp {linenos=table}
#include <iostream>

int main()
{
	std::cout << "Hello, world!\n";

	return 0;
}
```

This code will simply print the phrase `Hello, world!` to the console before exiting, and will be here simply to test that our project will build correctly.

### Run CMake
#### Windows

#### Linux
Create a folder called `build` in the main project directory. Open the `build` directory in a terminal and run the command `cmake ..`. This will run CMake using the CMakeLists.txt we created earlier.

## Time to build!
Finally, build the project:

### Windows
  * Visual Studio: Click `Build` in the top menu
  * VSCode: Run the `Build` task

### Linux
In a terminal, run `make` which will build the output executable `tutorial`.

### Run the program!
Run the executable file and you should see `Hello, world!` in the console.[^1]

## Next
Now that we have made sure that the project builds and links properly, in the next tutorial we will handle creating a libtcod window, rendering tiles, and handling input.

[visual-studio-community]: https://devblogs.microsoft.com/cppblog/getting-started-with-visual-studio-for-c-and-cpp-development/
[vscode]: https://code.visualstudio.com/docs/languages/cpp
[cmake]: https://cmake.org/download/
[libtcod-download]: https://github.com/libtcod/libtcod/releases
[libtcod-source]: https://github.com/libtcod/libtcod
[^1]: In Windows, you will likely need to use the command line or a PowerShell window to see the output from the program. Visual Studio and VSCode both have a console built-in to the IDE that may be used.
