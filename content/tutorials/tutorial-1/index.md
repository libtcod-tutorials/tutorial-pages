---
title: "Tutorial 1"
summary: "Basic libtcod window and movement"
date: 2023-06-23
draft: false
---

In this tutorial we will do the basic libtcod window setup and the main game loop with a simple player representation and movement.

## Main Loop
The main loop is a high level structure that most interactive programs follow. For a game it generally looks like this.[^1]

```cpp
// main
//    initialize
//
//    while running
//        get input
//        update game state
//        render to screen
//
//    cleanup
// end
```

We will implement each of the steps in the main loop using functions from the libtcod framework. But first, let's change some things in our `main.cpp` to get ready. 

Remove the `iostream` include and the `"Hello, world!"` lines. Then include two headers from `libtcod`, `console.hpp` and `sys.hpp`, which hold the function definitions we need for the rest of this tutorial. Additionally, include the `string_view` library, which we will use for the title of the game window.

#### `main.cpp`
```cpp {linenos=table}
#include <libtcod/console.hpp>
#include <libtcod/sys.hpp>

#include <string_view>

int main()
{
    return 0;
}
```

Beautiful simplicity.

### Initialize
The very first thing that needs to be done to use libtcod is to initialize the library, through the use of `TCODConsole::initRoot`[^2]. This is a static function that starts the libtcod window, with the following definition:

```cpp
static void TCODConsole::initRoot (int w, int h, const char * title, bool fullscreen = false, TCOD_renderer_t renderer = TCOD_RENDERER_SDL)
```

We will supply this with three arguments (since the last two are defaulted): the width and height of the window, and the title that the window displays. The width and the height arguments do not mean in pixels, but instead in cells like a grid. For these tutorials we will be using a screen size of 80x50.

In our main function, create variables for the width, height, and title, and then pass them into the `TCODConsole::initRoot` function. We can use `std::string_view` for the title and pass that to the function using the `data()` method. Since these variables won't change after we declare them, we can also declare the width, height, and title all as `constexpr`.

After that, declare two `int` variables called `x` and `y`. These will represent the location in the game window of our player. Assign these the values to `window_width / 2` and `window_height / 2`, respectively, which will put our starting position in the middle of the screen.

Then we will create a while loop, which will contain the rest of the main loop. The parameter for the loop will be `running`, which we create before the loop and set to `true`. Later we will check if the user wants to quit and set `running` to false if so, exiting the loop and quitting the program.

#### `main.cpp`
```cpp {linenos=table}
int main()
{
    constexpr int window_width = 80;
    constexpr int window_height = 50;
    constexpr std::string_view window_title = "libtcod C++ tutorial";

    // initialize
    TCODConsole::initRoot(window_width, window_height, window_title.c_str());

    int x = window_width / 2;
    int y = window_height / 2;

    bool running = true;

    while (running)
    {
        // ...
    }
}
```

### Input
Next, we will need to take in user input to update the player position on the screen.

```cpp
typedef struct {
    TCOD_keycode_t vk;
    char c;
    char text[32];
    bool pressed;
    bool lalt;
    bool lctrl;
    bool lmeta;
    bool ralt;
    bool rctrl;
    bool rmeta;
    bool shift;
} TCOD_key_t;
```

`TCOD_key_t` is a struct that contains information about a keyboard button. We will make a variable called `key` to store the key and use that to determine where to move.

```cpp
static TCOD_event_t TCODSystem::waitForEvent(int eventMask, TCOD_key_t *key, TCOD_mouse_t *mouse, bool flush)
```

`TCODSystem::waitForEvent` is a static function that will wait (block) for a user event determined by the `eventMask` parameter; we will use `TCOD_EVENT_KEY_PRESS` for this parameter. We will pass in the `key` parameter to this function, and leave the `mouse` parameter as a `nullptr` since we only are concerned with keys being pressed. Note that the `key` and `mouse` parameters are passed in as references, so after calling the function `key` will have information about what key has been pressed.

```cpp {linenos=table}
// input
TCOD_key_t key {}; // create a key object
TCODSystem::waitForEvent(TCOD_EVENT_KEY_PRESS, &key, nullptr, false); // wait for user to press a key
```

### Update
After checking for input, we will need to update the player position. We will do this by checking what key has been pressed, and update the position variables that we created in the `Initialize` section.

In `TCOD_key_t`, we check the `vk` member variable, which contains a keycode for what key has been pressed. In our case, we'll check for `TCODK_UP`, `TCODK_DOWN`, `TCODK_LEFT`, and `TCODK_RIGHT`, which correspond to the up, down, left, and right arrow buttons on the keyboard (respectively).

Make a `switch` using `key.vk`, with each case being one of the `TCODK_*` codes. In the up and down cases, modify the `y` variables, subtracting from it for up and adding to it for down. Then in the left and right cases, do the same for the `x` variable, subtracting on left and adding on right. If the user hits the Escape key we want the program to exit, so set the `running` variable to `false` in this case. For the default case, simply break since we won't be handling any other actions at this time.

```cpp {linenos=table}
// update
switch (key.vk)
{
    case TCODK_UP:
        --y;
        break;
    case TCODK_DOWN:
        ++y;
        break;
    case TCODK_LEFT:
        --x;
        break;
    case TCODK_RIGHT:
        ++x;
        break;
    case TCODK_ESCAPE:
        running = false;
        break;
    default:
        break;
}
```

### Render
In order to actually view our game, we will need to render it to the screen. In most games, rendering happens per frame, with a number of frames happening per second (FPS), with the standard in modern games being at least 60 FPS. In our case, we will only render to the screen when the user presses a key.

First, we will call `TCODConsole::clear`, a static function that will reset everything in the console to a single color, defaulting to black.

```cpp
void TCODConsole::putChar(int x, int y, int c, TCOD_bkgnd_flag_t flag = TCOD_BKGND_DEFAULT)
```

Next, we use `TCODConsole::putChar` to render a character at a given `x` and `y` position on the screen. In our case, we will put the `@` symbol, the traditional icon of the player in roguelikes, at our `x` and `y` position variables.

Finally, we need to call `TCODConsole::flush`, which will take care of actually drawing everything to the screen.

```cpp {linenos=table}
// render
TCODConsole::root->clear(); // Clear the console before we render anything
TCODConsole::root->putchar(x, y, '@'); // Render an @ at the player position
TCODConsole::root->flush(); // Flushing the console redraws it to the screen
```

(Note that in `Putting it all together` below, the `Render` sections occurs before the `Input` and `Update` steps; this is so that the initial state of the game will render before needing any user input, otherwise it would just show a blank window until a key is pressed.)

### Cleanup
Finally, outside the while loop we will call `TCOD_quit()`, which will deallocate any resources that TCOD has created before ending the program. It is not strictly necessary to do this, as most modern operating systems will automatically reclaim any memory used after a program ends, but it is still a good practice to remember.

### Putting it all together
Combining all the code above into our `main.cpp` file, we should have this:

#### `main.cpp`
```cpp {linenos=table}
#include <libtcod/console.hpp>
#include <libtcod/sys.hpp>

#include <string_view>

int main()
{
    constexpr int window_width = 80;
    constexpr int window_height = 50;
    constexpr std::string_view window_title = "libtcod C++ tutorial";

    // initialize
    TCODConsole::initRoot(window_width, window_height, window_title.c_str());

    // Player start position
    int x = window_width / 2;
    int y = window_height / 2;

    bool running = true;

    // main game loop
    while (running)
    {
        // render
        TCODConsole::root->clear(); // Clear the console before we render anything
        TCODConsole::root->putchar(x, y, '@'); // Render an @ at the player position
        TCODConsole::root->flush(); // Flushing the console redraws it to the screen

        // input
        TCOD_key_t key {}; // create a key object
        TCODSystem::waitForEvent(TCOD_EVENT_KEY_PRESS, &key, nullptr, false); // wait for user to press a key

        // update
        switch(key.vk)
        {
            case TCODK_UP:
                --y;
                break;
            case TCODK_DOWN:
                ++y;
                break;
            case TCODK_LEFT:
                --x;
                break;
            case TCODK_RIGHT:
                ++x;
                break;
            case TCODK_ESCAPE:
                running = false;
                break;
            default:
                break;
        }
    }

    // cleanup
    TCOD_quit(); // call before exit to clean up tcod resources

    return 0;
}
```

Now if we compile and run, it should show a window that contains a black screen with a white `@` in the center, and that you can move around with the arrow keys! It should look something like this:

![Basic view](basic-view.png)

---

## Breaking it down
So far, we have the basic functionality for a roguelike game down, but over the course of these tutorials we will want to extend it. In order to do this, we should break the code down into smaller, reusable functions. This will still follow the basic format that was outlined at the beginning.

#### `main.cpp`
```cpp
init() { /*...*/ }
render() { /*...*/ }
input() { /*...*/ }
update() { /*...*/ }
cleanup() { /*...*/ }

int main()
{
    init();

    while (running)
    {
        render();
        input();
        update();
    }

    cleanup();
}
```

But consider how we will update the player position when a key is pressed. With this setup, we will have to store the button press and pass it to the `update()` function, as well as the player position to update. Then we would have to pass the player position to the rendering function. So instead, we should encapsulate both the position and button press event in a class that will also handle the rest of the main loop functionality. This will be the foundation for our game engine.

### The Engine
Create a new file called `Engine.hpp` in the `include` directory from the root directory of our project. In this file we define a class called `Engine`, with public methods for each of our functions described above. Rather than the `init()` and `cleanup()` functions, we will use the class constructor and destructor, following the concept of RAII[^3].

Create public methods for the constructor and destructor, as well as `Input`, `Update`, `IsRunning`, and `Render`. All of these should have `void` as their return values, except for `IsRunning`, which should return a `bool`. In addition, create a private method called `Quit`.

Mark the `IsRunning` and `Render` functions as `const` to signify that no class member variables should be changed during these functions.

#### `Engine.hpp`
```cpp {linenos=table}
#ifndef ENGINE_HPP
#define ENGINE_HPP

namespace tutorial
{
    class Engine
    {
        public:
            Engine();
            ~Engine();

            void Input();
            void Update();

            bool IsRunning() const;
            void Render() const;

        private:
            void Quit();
    };
}

#endif // ENGINE_HPP
```

Then in the `src` directory, we create the corresponding `Engine.cpp` file to put the class implementation. Include our new `Engine.hpp` file and copy over the `#include`s from `main.cpp` below that. Copy over the `Initialize` code into the `Engine` constructor, and copy the `Cleanup` code into the destructor. For the other functions, create their bodies but leave them empty for now.

#### `Engine.hpp`
```cpp {linenos=table}
#include "Engine.hpp"

#include <libtcod/console.hpp>
#include <libtcod/sys.hpp>

#include <string_view>

namespace tutorial
{
    Engine::Engine()
    {
        constexpr int window_width = 80;
        constexpr int window_height = 50;
        constexpr std::string_view window_title = "libtcod C++ tutorial";

        TCODConsole::initRoot(window_width, window_height, window_title.c_str());
    }

    Engine::~Engine()
    {
        TCOD_quit();
    }

    void Engine::Input()
    {
        // TODO
    }

    void Engine::Update()
    {
        // TODO
    }

    bool Engine::IsRunning() const
    {
        // TODO
    }

    void Engine::Render() const
    {
        // TODO
    }

    void Engine::Quit()
    {
        // TODO
    }
}
```

We will come back to fill in the rest of the methods later. First, we need to deal with storing the player's position and handling the user input.

### Position
For the position, we should create a struct to encapsulate the X and Y values. So create a file in `include` called `Position.hpp`, since this will be used throughout our code. Then create a `struct` containing the X and Y position.

#### `Position.hpp`
```cpp {linenos=table}
#ifndef POSITION_HPP
#define POSITION_HPP

namespace tutorial
{
    struct Position
    {
        int x;
        int y;
    };
}

#endif // POSITION_HPP
```

Very simple and straightforward. Since it's a struct, the `x` and `y` variables are publicly accessible by default, and it's extremely cheap to pass around.

Now we can store the player position in the `Engine` class more easily. Include the new Position header, and add a private member variable to the Engine class called `player_pos_`[^4].

#### `Engine.hpp`
```cpp {linenos=table,hl_lines=[4, 14]}
#ifndef ENGINE_HPP
#define ENGINE_HPP

#include "Position.hpp"

namespace tutorial
{
    class Engine
    {
        public:
            // ...

        private:
            Position player_pos_;
    };
}

#endif // ENGINE_HPP
```

Then in `Engine.cpp` add code to initialize the player position to the center of the screen. We can set it directly as a new `Position` struct.

#### `Engine.cpp`
```cpp {linenos=table,hl_lines=[9]}
Engine::Engine()
{
    constexpr int window_width = 80;
    constexpr int window_height = 50;
    constexpr std::string_view window_title = "libtcod C++ tutorial";

    TCODConsole::initRoot(window_width, window_height, window_title.c_str());

    player_pos_ = Position { window_width / 2, window_height / 2 };
}
```

### Events
The next step is to handle user input events. Notice that our `Input` and `Update` functions take no arguments and return nothing, because we will also store what event has been pressed in our `Engine` class.

In the `include` directory, create a new file `Event.hpp` and declare an `enum class` called `Event`. This will hold all the possible events that can happen in our game so far: Moving up, down, left, and right; quitting the game; and no event.

#### `Event.hpp`
```cpp {linenos=table}
#ifndef EVENT_HPP
#define EVENT_HPP

namespace tutorial
{
    enum class Event
    {
        None,
        MoveUp,
        MoveDown,
        MoveLeft,
        MoveRight,
        Quit
    };
}

#endif // EVENT_HPP
```

Include this newly created header in `Engine.hpp` and create a private member variable to hold the next event to process in the game loop. Also, create a `bool` private member variable called `running_`.

#### `Engine.hpp`
```cpp {linenos=table,hl_lines=[4,"16-17"]}
#ifndef ENGINE_HPP
#define ENGINE_HPP

#include "Event.hpp"
#include "Position.hpp"

namespace tutorial
{
    class Engine
    {
        public:
            // ...

        private:
            Position player_pos_;
            Event next_event_;
            bool running_;
    };
}

#endif // ENGINE_HPP
```

Now we can revisit the rest of the `Engine` class methods.

First, add the new `running_` member to the `Engine` constructor with the initializer list, setting it to true.

#### `Engine.cpp`
```cpp {linenos=table}
Engine::Engine() : running_(true)
{
    // ...
}
```

Then for `IsRunning`, return the value of `running_`.

```cpp {linenos=table}
bool Engine::IsRunning() const
{
    return running_;
}
```

Inside the `Quit` method, set the `running_` member variable to `false`.

```cpp {linenos=table}
void Engine::Quit()
{
    running_ = false;
}
```

Then we can copy over the `Input` and `Update` code into our new `Input` class method. Instead of directly updating the player position, however, we will instead update our `next_event_` class variable to the `Event` that we want to correspond to each key press; `Event::MoveUp` when pressing up, and so on. We will also want to handle quitting the game, so set the case of pressing the Escape key to `Event::Quit`. For the default case, set `next_event_` to `Event::None`.

```cpp {linenos=table}
void Engine::Input()
{
    TCOD_key_t key {}; // create a key object
    TCODSystem::waitForEvent(TCOD_EVENT_KEY_PRESS, &key, nullptr, false); // wait for user to press a key

    switch (key.vk)
    {
        case TCODK_UP:
            next_event_ = Event::MoveUp;
            break;
        case TCODK_DOWN:
            next_event_ = Event::MoveDown;
            break;
        case TCODK_LEFT:
            next_event_ = Event::MoveLeft;
            break;
        case TCODK_RIGHT:
            next_event_ = Event::MoveRight;
            break;
        case TCODK_ESCAPE:
            next_event_ = Event::Quit;
            break;
        default:
            next_event_ = Event::None;
            break;
    }
}
```

Next, copy over the `Update` code block into the `Update` class method. Change the switch to use the `next_event_` class variable, and change the cases to correspond to the `Event` cases; from `TCODK_RIGHT` to `Event::MoveRight`, etc. For `Event::Quit`, we want to call our `Quit` method.

```cpp {linenos=table}
void Engine::Update()
{
    switch (next_event_)
    {
        case Event::MoveUp:
            --player_pos_.y;
            break;
        case Event::MoveDown:
            ++player_pos_.y;
            break;
        case Event::MoveLeft:
            --player_pos_.x;
            break;
        case Event::MoveRight:
            ++player_pos_.x;
            break;
        case Event::Quit:
            Quit();
            break;
        default:
            break;
    }
}
```

Finally, we can copy over the `Render` code into our `Render` class method, changing the `x` and `y` variables in the `putchar` function call to use our new `player_pos_` member variable.

```cpp {linenos=table}
void Engine::Render() const
{
    TCODConsole::root->clear(); // Clear the console before we render anything
    TCODConsole::root->putchar(player_pos_.x, player_pos_.y, '@'); // Render an @ at the player position
    TCODConsole::root->flush(); // Flushing the console redraws it to the screen
}
```

Then we can return to `main.cpp` again and put everything together.

First, we remove our `#include`s and include `Engine.hpp` instead. Then we create a variable for our `Engine`, and call our class methods in the appropriate places. With our constructor and destructor for `Engine`, the TCOD window will be automatically created when the `engine` variable is created, and when the window is closed, `TCOD_quit` will be called automatically once the end of `main` is reached.

#### `main.cpp`
```cpp {linenos=table}
#include "Engine.hpp"

int main()
{
    tutorial::Engine engine {};

    while (engine.IsRunning())
    {
        engine.Render();
        engine.HandleInput();
        engine.Update();
    }

    return 0;
}
```

One final thing to do before compiling again is to update our `CMakeLists.txt` file to account for the new files we added. To the `add_executable` statement, add in the relative path to the `Engine.cpp` file underneath `main.cpp`, which should be `src/Engine.cpp`. In addition, to the `target_include_directories` statement add a new line with `PRIVATE include/` so that `CMake` can find our include directory with our header files. Going forward, we will need to add any new `.cpp` files to the `add_executable` command, but any includes we create will be automatically found.

```cmake {linenos=table,hl_lines=[6,11]}
# ...

# Add the target to create the executable from sources
add_executable(tutorial
    main.cpp
    src/Engine.cpp
)

# Add the include directory
target_include_directories(tutorial
    PRIVATE include/
    PRIVATE ${libtcod_INCLUDE_DIR}
)

# ...
```

Now when compiling and running our program, it should function the same as before, but now we have a much more modular base to build on for the rest of the tutorials.

---

## Next steps
In the next tutorial, we will cover creating a map out of tiles, collision detection, and multiple entities.

---

## Finalized code

#### `Position.hpp`
```cpp {linenos=table}
#ifndef POSITION_HPP
#define POSITION_HPP

namespace tutorial
{
    struct Position
    {
        int x;
        int y;
    };
}

#endif // POSITION_HPP
```

#### `Event.hpp`
```cpp {linenos=table}
#ifndef EVENT_HPP
#define EVENT_HPP

namespace tutorial
{
    enum class Event
    {
        None,
        MoveUp,
        MoveDown,
        MoveLeft,
        MoveRight,
        Quit
    };
}

#endif // EVENT_HPP
```

#### `Engine.hpp`
```cpp {linenos=table}
#ifndef ENGINE_HPP
#define ENGINE_HPP

#include "Event.hpp"
#include "Position.hpp"

namespace tutorial
{
    class Engine
    {
    public:
        Engine();
        ~Engine();

        void HandleInput();
        void Update();

        bool IsRunning() const;
        void Render() const;

    private:
        void Quit();

        Position player_pos_;
        Event next_event_;
        bool running_;
    };
}

#endif // ENGINE_HPP
```

#### `Engine.cpp`
```cpp {linenos=table}
#include "Engine.hpp"

#include "Event.hpp"
#include "Position.hpp"

#include <libtcod/console.hpp>
#include <libtcod/sys.hpp>

#include <string_view>

namespace tutorial
{
    Engine::Engine() : running_(true)
    {
        constexpr int window_width = 80;
        constexpr int window_height = 50;
        constexpr std::string_view window_title = "libtcod C++ tutorial";

        TCODConsole::initRoot(window_width, window_height, window_title.c_str());

        player_pos_ = Position { window_width / 2, window_height / 2 };
    }

    Engine::~Engine()
    {
        // Need to call this before we exit to clean tcod up
        TCOD_quit();
    }

    void Engine::HandleInput()
    {
        TCOD_key_t key {}; // create a key object
        TCODSystem::waitForEvent(TCOD_EVENT_KEY_PRESS, &key, nullptr, false); // wait for user to press a key

        switch (key.vk)
        {
            case TCODK_UP:
                next_event_ = Event::MoveUp;
                break;
            case TCODK_DOWN:
                next_event_ = Event::MoveDown;
                break;
            case TCODK_LEFT:
                next_event_ = Event::MoveLeft;
                break;
            case TCODK_RIGHT:
                next_event_ = Event::MoveRight;
                break;
            case TCODK_ESCAPE:
                next_event_ = Event::Quit;
                break;
            default:
                next_event_ = Event::None;
                break;
        }
    }

    void Engine::Update()
    {
        switch (next_event_)
        {
            case Event::MoveUp:
                --player_pos_.y;
                break;
            case Event::MoveDown:
                ++player_pos_.y;
                break;
            case Event::MoveLeft:
                --player_pos_.x;
                break;
            case Event::MoveRight:
                ++player_pos_.x;
                break;
            case Event::Quit:
                Quit();
                break;
            default:
                break;
        }
    }

    bool Engine::IsRunning() const
    {
        return running_;
    }

    void Engine::Render() const
    {
        TCODConsole::root->clear(); // Clear the console before we render anything
        TCODConsole::root->putchar(player_pos_.x, player_pos_.y, '@'); // Render an @ at the player position
        TCODConsole::root->flush(); // Flushing the console redraws it to the screen
    }

    void Engine::Quit()
    {
        running_ = false;
    }
} // namespace tutorial
```

#### `main.cpp`
```cpp {linenos=table}
#include "Engine.hpp"

int main()
{
    // Initialize the engine, which will create the tcod window
    tutorial::Engine engine {};

    while (engine.IsRunning())
    {
        engine.Render();        // render everything to the screen
        engine.HandleInput();   // get input from user
        engine.Update();        // do any game updates
    }

    return 0;
}
```

[game-loop]: https://gameprogrammingpatterns.com/game-loop.html
[raii]: https://en.cppreference.com/w/cpp/language/raii
[google-style-guide]: https://google.github.io/styleguide/cppguide.html
[tcod-init-root]: https://libtcod.github.io/docs/html2/console_init_root.html?c=false&cpp=true&cs=false&py=false&lua=false
[^1]: For more on game loops, check out the chapter in *Game Programming Patterns* by Bob Nystrom [here][game-loop].
[^2]: See the libtcod documentation on the function [here][tcod-init-root].
[^3]: RAII - Resource Acquisition Is Initialization, an idiom for resource allocation and deallocation. For more, check out the CppReference article [here][raii].
[^4]: This member variable declaration scheme is from the [Google C++ Style Guide][google-style-guide].
